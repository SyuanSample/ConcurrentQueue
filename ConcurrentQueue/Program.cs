﻿using System.Collections.Concurrent;
using ConcurrentQueue.Domain;
using ConcurrentQueue.Model;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using var host = Host.CreateDefaultBuilder(args)
                     .ConfigureServices
                     (
                         (host
                        , service) =>
                         {
                             service.AddMemoryCache();
                         }
                     )
                     .Build();

var cache = host.Services.GetService<IMemoryCache>();
var queue = new ConcurrentQueue<MemberParam?>();

//  產生資料流
for (var i = 0
     ; i <= 1000
     ; i++)
{
    var data = new MemberParam
    {
        MemberId = i, MemberAccount = "QueueTest" + i.ToString("0000"), Token = Guid.NewGuid()
            .ToString()
      , UserAgent = "UserAgent" + i.ToString("0000"), DeviceType = (i % 2).ToString(), WebMode = (short) (i % 3)
    };

    queue.Enqueue(data);
}

var myAction = () =>
{
    MemberParam? data;

    while (queue.TryDequeue(out data))
    {
        CreateCacheMember(data);
        CreateCacheLog(data);
    }
};

{
    Parallel.Invoke(myAction, myAction, myAction, myAction);
}


var memberCount    = CacheMemberCount();
var memberLogCount = CacheLogCount();

Console.WriteLine($"Member資料目前 {memberCount.ToString("0000")} 筆, Log資料目前 {memberLogCount.ToString("0000")} 筆");
Console.ReadKey();

void CreateCacheMember(MemberParam? memberParam)
{
    #region Data Init

    var member = new Member
    {
        Id         = memberParam.MemberId, Account     = memberParam.MemberAccount, CreateDatetime = DateTime.Now
      , DeviceType = memberParam.DeviceType, UserAgent = memberParam.UserAgent, WebMode            = memberParam.WebMode
    };

    #endregion




    #region CacheSetting
    var cacheKey = "Member";

    var cacheEntryOptions = new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromMinutes(10));

    #endregion

    #region Cache Insert

    if (!cache.TryGetValue(cacheKey, out List<Member> members))
    {
        members = new List<Member>
        {
            member
        };

        cache.Set(cacheKey, members, cacheEntryOptions);

        return;
    }


    if (members.Count > 996)
    {
        Console.WriteLine(member.Account);
        Console.WriteLine($"準備寫入地{members.Count + 1}筆");
    }
    members.Add(member);
    cache.Set(cacheKey, members, cacheEntryOptions);

    #endregion
}

;

int CacheMemberCount()
{
    #region CacheSetting

    var cacheKey = "Member";

    var cacheEntryOptions = new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromMinutes(10));

    #endregion

    #region Cache Count

    if (!cache.TryGetValue(cacheKey, out List<Member> members))
    {
        return 0;
    }

    return members.Count;

    #endregion
};


void CreateCacheLog(MemberParam? memberParam)
{
    #region Data Init

    var member = new MemberLoginLog
    {
        Id         = memberParam.MemberId, CreateDatetime = DateTime.Now
      , DeviceType = memberParam.DeviceType, UserAgent    = memberParam.UserAgent, WebMode = memberParam.WebMode
    };

    #endregion

    #region CacheSetting

    var cacheKey = "MemberLog";

    var cacheEntryOptions = new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromMinutes(10));

    #endregion

    #region Cache Insert

    if (!cache.TryGetValue(cacheKey, out List<MemberLoginLog> members))
    {
        members = new List<MemberLoginLog>
        {
            member
        };

        cache.Set(cacheKey, members, cacheEntryOptions);

        return;
    }

    if (members.Count > 996)
    {
        Console.WriteLine($"準備寫入地{members.Count + 1}筆");
    }

    members.Add(member);
    cache.Set(cacheKey, members, cacheEntryOptions);

    #endregion
};

int CacheLogCount()
{
    #region CacheSetting

    var cacheKey = "MemberLog";

    #endregion

    #region Cache Insert

    if (!cache.TryGetValue(cacheKey, out List<MemberLoginLog> members))
    {
        return 0;
    }

    return members.Count;

    #endregion
}

;