﻿namespace ConcurrentQueue.Model;

internal class MemberParam
{
    /// <summary>
    ///     會員帳號
    /// </summary>
    public long MemberId { get; set; }

    /// <summary>
    ///     會員帳號
    /// </summary>
    public string MemberAccount { get; set; }

    /// <summary>
    ///     登入Token
    /// </summary>
    public string Token { get; set; }

    /// <summary>
    ///     UserAgent
    /// </summary>
    public string UserAgent { get; set; }

    /// <summary>
    ///     DeviceType
    /// </summary>
    public string DeviceType { get; set; }

    /// <summary>
    ///     WebMode
    /// </summary>
    public short WebMode { get; set; }
}