﻿#nullable disable

namespace ConcurrentQueue.Domain;

public class Member
{
    public long Id { get; set; }

    public string Account { get; set; }

    public DateTime? CreateDatetime { get; set; }
    
    public string DeviceType { get; set; }

    public string UserAgent { get; set; }

    public short? WebMode { get; set; }
}