﻿namespace ConcurrentQueue.Domain;

public class MemberLoginLog
{
    public long Id { get; set; }

    public DateTime CreateDatetime { get; set; }

    public string DeviceType { get; set; }

    public short? WebMode { get; set; }

    public string UserAgent { get; set; }
}